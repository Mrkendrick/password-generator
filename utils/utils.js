import fs from 'fs'
import path from 'path'
import os from 'os'
import chalk from 'chalk'

class Utils {
  constructor() {
    this.alpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    this.numbers = '0123456789'
    this.symbols = '!@#$%^&*_-+='
    this.log = console.log
  }

  createPassword(length = 8, hasNumbers = true, hasSymbols = true) {
    let chars = this.alpha
    hasNumbers ? (chars += this.numbers) : ''
    hasSymbols ? (chars += this.symbols) : ''
    return this.generatedPassword(length, chars)
  }

  generatedPassword(length, chars) {
    let password = ''
    for (let i = 0; i < length; i++) {
      password += chars.charAt(Math.random() * chars.length)
    }

    return password
  }

  savePassword(password) {
    fs.open(
      path.join(__dirname, '../', 'passwords.txt', 'a', 666, (e, id) => {
        fs.write(id, password + os.EOL, null, 'utf-8', () => {
          fs.close(id, () => {
            this.log(chalk.green('Password saved to password.txt'))
          })
        })
      }),
    )
  }
}

const UtilityService = new Utils()

export default UtilityService
