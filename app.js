#!/usr/bin/env node
import chalk from 'chalk'
import { program } from 'commander'
import clipboardy from 'clipboardy'
import UtilityService from './utils/utils.js'

const log = console.log

program.version('1.0.0').description('Password Generator')

program
  .option('-l, --length <number>', 'length of password', 8)
  .option('-nn, --no-numbers', 'remove numbers')
  .option('-ns, --no-symbols', 'remove symbols')
  .parse()

const { length, numbers, symbols } = program.opts()

const generatedPassword = UtilityService.createPassword(
  length,
  numbers,
  symbols,
)

clipboardy.writeSync(generatedPassword)

log(chalk.blue('Generated Password: ') + chalk.bold(generatedPassword))
log(chalk.yellow('Password copied to clipboard'))
